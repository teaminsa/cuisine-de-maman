class LemonWay
  def self.post(methodName, content)
    RestClient.post APP_CONFIG["lemon_way_devkit"], self.body(methodName, content), content_type: "text/xml", accept: "application/xml"
  end

  private

  def self.body(methodName, content)
    content[:wlLogin] = APP_CONFIG["lemon_way_account"]
    content[:wlPass] = APP_CONFIG["lemon_way_password"]
    content[:language] = APP_CONFIG["lemon_way_language"]
    content[:version] = APP_CONFIG["lemon_way_version"]

    options = { indent: 0, skip_types: true }
    builder = Builder::XmlMarkup.new(options)
    options[:builder] = builder

    builder.instruct! :xml, :version => "1.0", :encoding => "utf-8"
    builder.soap12 :Envelope, "xmlns:xsi" => "http://www.w3.org/2001/XMLSchema-instance", "xmlns:xsd" => "http://www.w3.org/2001/XMLSchema", "xmlns:soap12" => "http://www.w3.org/2003/05/soap-envelope" do
      builder.soap12 :Body do
        builder.tag! methodName, xmlns: "Service_mb" do
          content.each do |key, value|
            ActiveSupport::XmlMini.to_tag key, value, options
          end
        end
      end
    end
  end
end