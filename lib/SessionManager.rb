require "EncryptionManager.rb"

class SessionManager
  #abstract class
  def initialize
    raise "Doh! You are trying to instantiate an abstract class!"
  end

  def self.connection(email, password)
    user = User.find_by_email email
    if !user.nil? and !user.password.nil? and EncryptionManager.hashPassword(password, user.salt) == user.password
      self.newSession user
    end
  end

  def self.connectionFacebook(accessToken)
    RestClient.get "https://graph.facebook.com/me", {:params => {:access_token => accessToken, :fields => "id"}} do |response, request, result|
      if response.code == 200
        data = JSON.parse response
        userFacebook = UserFacebook.find_by_facebook_user_id data["id"]
        if !userFacebook.nil?
          user = userFacebook.user
          if !user.nil?
            self.newSession user
          end
        end
      end
    end
  end

  def self.getSession(userId, token)
    session = Session.find_by_user_id_and_token userId, token
    if !session.nil?
      if Time.now > session.created_at + 24.hours
        #close the session if it's older than one day
        session.destroy
      elsif Time.now > session.updated_at + 30.minutes
        #session expired - no activity during 30 minutes
        session.destroy
      else
        #session still in use
        session.update_attribute :updated_at, Time.now
        return session
      end
    end
  end

  private

  def self.newSession(user)
    token = EncryptionManager.sessionToken()
    Session.create token: token, user: user
  end
end