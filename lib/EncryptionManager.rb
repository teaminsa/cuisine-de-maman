require "securerandom"

class EncryptionManager
  #abstract class
  def initialize
    raise "Doh! You are trying to instantiate an abstract class!"
  end

  # Hash password
  def self.hashPassword(password, salt)
    Digest::SHA2.new().digest password + salt
  end

  # Generate a salt for hashing password
  def self.salt
    SecureRandom.random_bytes 8
  end

  # Generate a session token
  def self.sessionToken
    SecureRandom.hex 16
  end

end
