# This is my README
This website is a service of healthy food delivery available across the United States and France.
Have a look of the result on [My Cuistot](https://www.mycuistot.com) !

Our goal with My Cuistot is to finally make possible to eat healthy and varied when you don’t have time or talent to go find recipes, go shopping, cooking and cleaning. Everybody knows the time it takes to do all this shit and how difficult it is to do it after a long day of work.
On another hand you have plenty of professionals, talented and motivated by cooking, who are working in poor conditions in restaurants. They work long hours, the kitchens are hot and stressful, and many hours are never paid. Working with us chef are perfectly paid, based on their results and efficiency, and they work from home with the organization they want.
Our chefs can make $300 /day working with us, 4 days a week. Working at full time with us, from Monday to Thursday they can easily make $4800 / month and live without stress and problems.

Next project of the team is  [Lombrosa Yacht](http://www.lombrosayacht.com) ! 
It will be the first marketplace listing all the yachts available for sale in the world.

Lombrosa is dedicated to yacht owners who want to sell their yacht quickly, at a good price. Through a direct contact with our local yacht brokers we put online their listing to make it visible by our potential buyers. 
Yachts for sale are listed by country and city on our website which make really easy the search of the yacht of their dream by our potential yacht buyers.
You can find all the luxury brands of yacht on our website like Nobiskrug, Oceanco or Perini Navi. Some interiors are designed by Gucci or Dior and engines constructed by BMW.
What give the best chances to success to Lombrosa is its dedicated team with a strong Chief Marketing Officer from My Cuistot and technical experts from the most famous constructors. All the yachts are checked up before going for sale, and our local brokers, partners and not employees, do the rest of the job.
Follow this project it could become huge!
