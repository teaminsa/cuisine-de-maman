Rails.application.routes.draw do
  resources :ratings

  scope 'api', defaults: { format: 'json' } do

    resources :availabilities, :dish_types, :pictures

    resources :dish_availabilities
    resources :command_dishes

    resources :dishes, only: [:index, :show] do
      collection do
        get 'search'
        get 'cuisine_keywords'
      end
    end

    resources :users do

      resources :availabilities do
        member do
          post 'dishes/:dish_id' => :add_dish
          delete 'dishes/:dish_id' => :remove_dish
        end
      end

      resources :commands do
        member do          
          get 'paymentUrl'
          get 'checkPaymentStatus'
          post 'rate' => 'ratings#rate_cook'
          post '/dishes/:dish_id/rate' => 'ratings#rate_dish'
          put 'delivered'
        end
        resources :command_dishes do
          
        end
      end

      resources :dishes do
        member do
          post 'add_picture'           
        end
      end

      resources :locations

      resources :orders do
        put 'accept'
        put 'decline'
        put 'gift'
        post 'rate' => 'ratings#rate_client'
      end
      
    end

    post 'commands/reschedule' => 'commands#reschedule'
    post 'sessions' => 'sessions#connect'
    get 'sessions/:user_id' => 'sessions#validate'
    delete 'sessions/:user_id' => 'sessions#destroy'

  end

  get '*path' => 'static#home'
  root 'static#home'
end