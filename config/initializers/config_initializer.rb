APP_CONFIG = YAML.load_file(Rails.root.join("config","config.yml"))[Rails.env]

Geocoder.configure(

  # set default units to kilometers:
  :units => :km,
)