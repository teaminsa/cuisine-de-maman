#encoding: utf-8 
# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#

require "EncryptionManager.rb"

#Creation of the command statuses
CommandStatus.create(status: "PENDING")
CommandStatus.create(status: "PAID")
CommandStatus.create(status: "ACCEPTED")
CommandStatus.create(status: "DECLINED")
CommandStatus.create(status: "DELIVERED")

#Creation of cuisine keywords
CuisineKeyword.create(keyword: "ASIAN")
french = CuisineKeyword.create(keyword: "FRENCH")
italian = CuisineKeyword.create(keyword: "ITALIAN")
CuisineKeyword.create(keyword: "CHINESE")
CuisineKeyword.create(keyword: "ORIENTAL")
CuisineKeyword.create(keyword: "MEXICAN")

#Creation of the dish types
DishType.create(name: "FISH")
meat = DishType.create(name: "MEAT")
vegan = DishType.create(name: "VEGAN")
desert = DishType.create(name: "DESERT")

#Additionnal testing data

salt = EncryptionManager.salt
password = EncryptionManager.hashPassword "pwd", salt


alice = User.create(email: "alice@bloutiouf.com", password: password, salt: salt, telephone: "0789237362", last_name: "Adams", first_name: "Alice")
alice.build_main_picture(picture: Picture.new(), user: alice)

aubergine = Dish.new(name: "Aubergine à l'ail", description: "C'est appétissant !", price: "6.2", dish_type: vegan)
aubergine.cuisine_keywords << french
aubergine.cuisine_keywords << italian
aubergine.build_main_picture(picture: Picture.new(), dish: aubergine)
alice.dishes << aubergine

avocado = Dish.new(name: "Avocat aux asperges", description: "C'est affamant !", price: "8.4", dish_type: vegan)
avocado.cuisine_keywords << french
avocado.build_main_picture(picture: Picture.new(), dish: avocado)
alice.dishes << avocado

alice.location = Location.new(latitude: 48.872342, longitude: 2.296220, city: "Paris", address: "77 avenue Marceau")
alice.wallet =  Wallet.new(walletIP: "127.0.0.1") 
alice.save

availability = Availability.new(start: Time.now + 1.days, end: Time.now + 1.days + 2.hours, user: alice)
DishAvailability.create(dish: aubergine, availability: availability)
DishAvailability.create(dish: avocado, availability: availability)


bob = User.create(email: "bob@bloutiouf.com", password: password, salt: salt, telephone: "0789237362", last_name: "Bain", first_name: "Bob")
bob.build_main_picture(picture: Picture.new(), user: bob)

beef = Dish.new(name: "Boeuf bourguignon", description: "C'est bon !", price: "7.5", dish_type: meat)
beef.build_main_picture(picture: Picture.new(), dish: beef)
bob.dishes << beef

beet = Dish.new(name: "Beignets de betteraves", description: "C'est bien !", price: "4.6", dish_type: desert)
beet.build_main_picture(picture: Picture.new(), dish: beet)
bob.dishes << beet

bob.location = Location.new(latitude: 48.853025, longitude: 2.369671, city: "Paris", address: "2 Place de la Bastille")
bob.wallet =  Wallet.new(walletIP: "127.0.0.1") 
bob.save

availability = Availability.new(start: Time.now + 1.days + 1.hours, end: Time.now + 1.days + 3.hours, user: bob)
DishAvailability.create(dish: beef, availability: availability)
DishAvailability.create(dish: beet, availability: availability)
