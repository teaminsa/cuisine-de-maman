# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140610201948) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "availabilities", force: true do |t|
    t.datetime "start"
    t.datetime "end"
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "command_dishes", force: true do |t|
    t.integer  "command_id"
    t.integer  "dish_id"
    t.integer  "amount"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "command_statuses", force: true do |t|
    t.string   "status"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "commands", force: true do |t|
    t.decimal  "price",             precision: 5, scale: 2
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "command_status_id"
    t.integer  "cook_id"
    t.datetime "delivery_datetime"
    t.integer  "availability_id"
    t.integer  "user_rating_id"
    t.integer  "cook_rating_id"
  end

  add_index "commands", ["command_status_id"], name: "index_commands_on_command_status_id", using: :btree
  add_index "commands", ["cook_id"], name: "index_commands_on_cook_id", using: :btree

  create_table "cuisine_keywords", force: true do |t|
    t.string   "keyword"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "dish_availabilities", force: true do |t|
    t.integer  "dish_id"
    t.integer  "availability_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "dish_cuisine_keywords", force: true do |t|
    t.integer  "dish_id"
    t.integer  "cuisine_keyword_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "dish_pictures", force: true do |t|
    t.integer "dish_id"
    t.integer "picture_id"
  end

  create_table "dish_ratings", force: true do |t|
    t.integer  "rating_id"
    t.integer  "command_dish_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "dish_types", force: true do |t|
    t.string "name"
  end

  create_table "dishes", force: true do |t|
    t.decimal  "price",           precision: 5, scale: 2
    t.text     "description"
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "delivery"
    t.boolean  "home_cooking"
    t.string   "name"
    t.integer  "dish_type_id"
    t.integer  "main_picture_id"
    t.decimal  "average_rating",  precision: 3, scale: 2
  end

  add_index "dishes", ["dish_type_id"], name: "index_dishes_on_dish_type_id", using: :btree
  add_index "dishes", ["main_picture_id"], name: "index_dishes_on_main_picture_id", using: :btree

  create_table "locations", force: true do |t|
    t.integer  "user_id"
    t.float    "latitude"
    t.float    "longitude"
    t.string   "city"
    t.string   "address"
    t.string   "informations"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "payments", force: true do |t|
    t.integer  "command_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "transactionId"
  end

  create_table "pictures", force: true do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "file_file_name"
    t.string   "file_content_type"
    t.integer  "file_file_size"
    t.datetime "file_updated_at"
  end

  create_table "ratings", force: true do |t|
    t.integer  "value"
    t.string   "comments"
    t.integer  "rater_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "sessions", force: true do |t|
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "token"
  end

  create_table "user_facebooks", force: true do |t|
    t.integer "user_id"
    t.string  "facebook_user_id"
  end

  add_index "user_facebooks", ["facebook_user_id"], name: "index_user_facebooks_on_facebook_user_id", using: :btree

  create_table "user_pictures", force: true do |t|
    t.integer "user_id"
    t.integer "picture_id"
  end

  create_table "user_ratings", force: true do |t|
    t.integer  "rating_id"
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "users", force: true do |t|
    t.string   "email"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "main_picture_id"
    t.string   "telephone"
    t.string   "last_name"
    t.string   "first_name"
    t.binary   "password"
    t.binary   "salt"
    t.string   "gender"
    t.decimal  "average_rating",  precision: 3, scale: 2
  end

  add_index "users", ["main_picture_id"], name: "index_users_on_main_picture_id", using: :btree

  create_table "wallets", force: true do |t|
    t.integer  "LWID"
    t.string   "walletIP"
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

end
