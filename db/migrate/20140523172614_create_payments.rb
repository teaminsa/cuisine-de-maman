class CreatePayments < ActiveRecord::Migration
  def change
    create_table :payments do |t|
      t.integer :transactionId
      t.belongs_to :command
      t.timestamps
    end
  end
end
