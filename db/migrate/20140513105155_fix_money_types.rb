class FixMoneyTypes < ActiveRecord::Migration
  def up
    change_column :commands, :price, :decimal, precision: 5, scale: 2
    change_column :dishes, :price_portion, :decimal, precision: 5, scale: 2
  end

  def down
    change_column :commands, :price, :float
    change_column :dishes, :price_portion, :float
  end
end
