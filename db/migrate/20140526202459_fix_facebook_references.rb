class FixFacebookReferences < ActiveRecord::Migration
  def change
    remove_reference :users, :user_facebook, index: true
    add_index :user_facebooks, :facebook_user_id
  end
end
