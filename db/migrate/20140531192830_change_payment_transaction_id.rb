class ChangePaymentTransactionId < ActiveRecord::Migration
  def change
    remove_column :payments, :transactionId, :integer    
    add_column :payments, :transactionId, :string
  end
end
