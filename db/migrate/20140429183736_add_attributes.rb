class AddAttributes < ActiveRecord::Migration
  def change
    add_column :plates, :delivery, :boolean
    add_column :plates, :home_cooking, :boolean
  end
end
