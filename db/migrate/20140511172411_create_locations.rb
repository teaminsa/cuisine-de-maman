class CreateLocations < ActiveRecord::Migration
  def change
    create_table :locations do |t|
      t.belongs_to :user
      t.float :latitude
      t.float :longitude
      t.string :city
      t.string :address
      t.string :informations
      t.timestamps
    end
  end
end
