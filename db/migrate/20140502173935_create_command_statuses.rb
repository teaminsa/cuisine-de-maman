class CreateCommandStatuses < ActiveRecord::Migration
  def change
    create_table :command_statuses do |t|
      t.string :status
      t.timestamps
    end
    
    remove_column :commands, :status_id, :integer
    
    add_reference :commands, :command_status, index: true
    
  end
end
