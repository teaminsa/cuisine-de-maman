class RemoveFilenameFromPicture < ActiveRecord::Migration
  def change
    remove_column :pictures, :filename, :string
  end
end
