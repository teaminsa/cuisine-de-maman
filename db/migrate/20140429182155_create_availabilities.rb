class CreateAvailabilities < ActiveRecord::Migration
  def change
    create_table :availabilities do |t|
      t.timestamp :debut
      t.timestamp :fin
      t.belongs_to :user
      
      t.timestamps
    end
    
  end
  
  
  
end
