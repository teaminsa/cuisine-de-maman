class ChangeTransactionIdInteger < ActiveRecord::Migration
  def change
    remove_column :payments, :transactionId, :string
    add_column :payments, :transactionId, :integer
  end
end
