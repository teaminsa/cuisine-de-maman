class ChangeColumnTokenSession < ActiveRecord::Migration
  def change

    remove_column :sessions, :token, :integer
    
    add_column :sessions, :token, :string
    
  end
end
