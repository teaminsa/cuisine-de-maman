class CreateWallets < ActiveRecord::Migration
  def change
    create_table :wallets do |t|
      t.integer :LWID
      t.string :walletIP
      t.belongs_to :user
      t.timestamps
    end
    
    add_column :users, :gender, :string
    rename_column :users, :nickname, :firstname
    rename_column :users, :name, :lastname
  end
end
