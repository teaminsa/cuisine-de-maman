class ChangeColumns < ActiveRecord::Migration
  def change
    change_table :availabilities do |t|
      t.rename :debut, :begin
      t.rename :fin, :end
    end
  end
end
