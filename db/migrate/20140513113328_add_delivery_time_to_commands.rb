class AddDeliveryTimeToCommands < ActiveRecord::Migration
  def change
    add_column :commands, :delivery_datetime, :datetime
  end
end
