class AddRatingToUserAndDish < ActiveRecord::Migration
  def change
    add_column :dishes, :average_rating, :decimal, precision: 3, scale: 2
    add_column :users, :average_rating, :decimal, precision: 3, scale: 2
  end
end
