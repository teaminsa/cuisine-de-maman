class FixCuisineKeywords < ActiveRecord::Migration
  def change
    rename_column :dish_cuisine_keywords, :cooking_style_id, :cuisine_keyword_id
  end
end
