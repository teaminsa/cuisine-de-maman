class CreateUserFacebooks < ActiveRecord::Migration
  def change
    create_table :user_facebooks do |t|
      t.integer :user_id
      t.string :facebook_user_id
    end

    add_reference :users, :user_facebook, index: true
    
  end
end
