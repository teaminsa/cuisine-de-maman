class CreateDishCookingStyles < ActiveRecord::Migration
  def change
    create_table :dish_cooking_styles do |t|
      t.belongs_to :dish
      t.belongs_to :cooking_style
      t.timestamps
    end
    
    remove_column :dishes, :cooking_style_id, :integer
  end
end
