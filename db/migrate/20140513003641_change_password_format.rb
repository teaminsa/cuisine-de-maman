class ChangePasswordFormat < ActiveRecord::Migration
  def up
    remove_column :users, :password
    add_column :users, :password, :binary, limit: 32
    add_column :users, :salt, :binary, limit: 8
  end

  def down
    remove_column :users, :password
    remove_column :users, :salt
    add_column :users, :password, :string
  end
end
