class RenameAmountInCommandDish < ActiveRecord::Migration
  def change
    rename_column :command_dishes, :nb_portions, :amount
  end
end
