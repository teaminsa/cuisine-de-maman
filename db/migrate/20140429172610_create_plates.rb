class CreatePlates < ActiveRecord::Migration
  def change
    create_table :plates do |t|
      t.float :price_portion
      t.text :description
      t.belongs_to :user
      
      t.timestamps
    end
  end
end
