class CreatePictures < ActiveRecord::Migration
  def change
    create_table :pictures do |t|
      t.string :url
      t.timestamps
    end
 
    create_table :dish_pictures do |t|
      t.belongs_to :dish
      t.belongs_to :picture
    end
    
    create_table :user_pictures do |t|
      t.belongs_to :user
      t.belongs_to :picture
    end

    add_reference :users, :main_picture, index: true
    add_reference :dishes, :main_picture, index: true
  end
end
