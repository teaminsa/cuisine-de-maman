class CreateDishRatings < ActiveRecord::Migration
  def change
    create_table :dish_ratings do |t|
      t.belongs_to :rating
      t.belongs_to :command_dish
      t.timestamps
    end
    
    
  end
end
