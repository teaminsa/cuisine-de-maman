class RenamePriceInDish < ActiveRecord::Migration
  def change
    rename_column :dishes, :price_portion, :price
  end
end
