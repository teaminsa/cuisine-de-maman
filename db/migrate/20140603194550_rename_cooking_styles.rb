class RenameCookingStyles < ActiveRecord::Migration
  def change
    rename_table :cooking_styles, :cuisine_keywords
    rename_table :dish_cooking_styles, :dish_cuisine_keywords
  end
end
