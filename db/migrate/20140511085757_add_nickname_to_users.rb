class AddNicknameToUsers < ActiveRecord::Migration
  def change
    remove_column :users, :firstname, :string
    remove_column :users, :family_name, :string
    add_column :users, :name, :string
    add_column :users, :nickname, :string
  end
end
