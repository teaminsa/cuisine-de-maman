class CreateRatings < ActiveRecord::Migration
  def change
    create_table :ratings do |t|
      t.integer :value
      t.string :comments
      t.integer :rater_id
      t.timestamps
    end
  end
end
