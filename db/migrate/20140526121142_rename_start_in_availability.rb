class RenameStartInAvailability < ActiveRecord::Migration
  def change
    rename_column :availabilities, :begin, :start
  end
end
