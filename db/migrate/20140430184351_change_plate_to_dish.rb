class ChangePlateToDish < ActiveRecord::Migration
  def change
    rename_table :plates, :dishes
    rename_table :command_plates, :command_dishes
    rename_table :plate_availabilities, :dish_availabitilies
 
  end
end
