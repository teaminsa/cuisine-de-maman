class CreateCommandPlates < ActiveRecord::Migration
  def change
    create_table :command_plates do |t|
      t.belongs_to :command
      t.belongs_to :plate
      t.integer :nb_portions
      
      t.timestamps
    end
  end
end
