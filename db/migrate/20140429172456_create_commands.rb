class CreateCommands < ActiveRecord::Migration
  def change
    create_table :commands do |t|
      t.float :price
      t.integer :status_id  
      t.belongs_to :user

      t.timestamps
    end
  end
end
