class CreateCookingStyles < ActiveRecord::Migration
  def change
    create_table :cooking_styles do |t|
      t.string :style
      t.timestamps
    end
    
    add_column :dishes, :cooking_style_id, :integer
  end
end
