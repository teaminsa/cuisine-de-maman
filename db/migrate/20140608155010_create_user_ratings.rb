class CreateUserRatings < ActiveRecord::Migration
  def change
    create_table :user_ratings do |t|
      t.belongs_to :rating
      t.belongs_to :user      
      t.timestamps
    end    
    add_column :commands, :user_rating_id, :integer
    add_column :commands, :cook_rating_id, :integer    
  end
end
