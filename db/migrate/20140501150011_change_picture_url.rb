class ChangePictureUrl < ActiveRecord::Migration
  def change
    rename_column :pictures, :url, :filename
  end
end
