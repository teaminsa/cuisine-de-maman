class ChangePlateToDish2 < ActiveRecord::Migration
  def change
     rename_table :dish_availabitilies, :dish_availabilities
    change_table :command_dishes do |t|
      t.rename :plate_id, :dish_id
    end
    
    change_table :dish_availabilities do |t|
      t.rename :plate_id, :dish_id
    end
    
  end
end
