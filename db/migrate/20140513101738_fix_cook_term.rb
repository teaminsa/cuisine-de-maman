class FixCookTerm < ActiveRecord::Migration
  def change
    remove_index :commands, :cooker_id
    rename_column :commands, :cooker_id, :cook_id 
    add_index :commands, :cook_id
  end
end
