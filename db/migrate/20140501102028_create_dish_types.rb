class CreateDishTypes < ActiveRecord::Migration
  def change
    create_table :dish_types do |t|
      
      t.string :name
    end
    
    add_reference :dishes, :dish_type, index: true
  end
end
