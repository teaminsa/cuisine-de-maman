class CreatePlateAvailabilities < ActiveRecord::Migration
  def change
    create_table :plate_availabilities do |t|
      t.belongs_to :plate
      t.belongs_to :availability

      t.timestamps
    end
  end
end
