class ChangeStyleToKeyword < ActiveRecord::Migration
  def change
    rename_column :cuisine_keywords, :style, :keyword
  end
end
