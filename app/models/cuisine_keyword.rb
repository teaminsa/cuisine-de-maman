class CuisineKeyword < ActiveRecord::Base
  has_many :dish_cuisine_keyword
  has_many :dishes, through: :dish_cuisine_keyword
  has_one :dish_rating
end
