class Dish < ActiveRecord::Base
  validates_presence_of :description, :dish_type, :name, :user
  validates_numericality_of :price, greater_than: 0

  alias_attribute :type, :dish_type

  belongs_to :user

  has_many :command_dishes, foreign_key: "dish_id"
  has_many :commands, through: :command_dishes

  has_many :dish_availabilities, class_name: "DishAvailability", foreign_key: "dish_id"
  has_many :availabilities, class_name: "Availability", through: :dish_availabilities

  belongs_to :dish_type

  has_many :dish_pictures

  belongs_to :main_picture, class_name: "DishPicture"
  
  has_many :dish_cuisine_keywords
  has_many :cuisine_keywords, through: :dish_cuisine_keywords
  
  def available_at(datetime)
    self.availabilities.each do |availability|
      if availability.start <= datetime and datetime < availability.end
        return availability
      end
    end
    nil
  end

  def available_between(interval_start, interval_end)
    self.availabilities.each do |availability|
      if availability.start < interval_end and interval_start < availability.end
        return availability
      end
    end
    nil
  end
  
  def first_availability
    availabilities = self.availabilities.all
    availabilities.sort_by {|availability| availability.start}
    time_now = Time.now
     for availability in availabilities
      if availability.end > Time.now
         return availability
      end
    end
  end
  
  def has_coming_availabilities    
    availabilities = self.availabilities.all
    if availabilities.length == 0
      return false
    else
      availabilities.sort_by {|availability| availability.end}
      if availabilities.last.end < Time.now
        return false
      else
        return true     
      end
    end

  end
  
end
