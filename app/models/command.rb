class Command < ActiveRecord::Base
  has_many :command_dishes
  has_many :dishes, through: :command_dishes, foreign_key: "command_id", class_name: "Command_dish"
  has_one :payment
  belongs_to :user
  belongs_to :cook, class_name: "User"
  belongs_to :command_status
  validates_numericality_of :price
  belongs_to :availability
  belongs_to :user_rating
  belongs_to :cook_rating, class_name: "UserRating"
end