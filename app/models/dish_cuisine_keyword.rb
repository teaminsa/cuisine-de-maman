class DishCuisineKeyword < ActiveRecord::Base
    belongs_to :dish
    belongs_to :cuisine_keyword
end
