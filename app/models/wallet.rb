class Wallet < ActiveRecord::Base
  belongs_to :user
  #register the wallet at LemonWay after the user creation
  after_create :register
  #update the wallet at LemonWay when the user's info are changed
  after_update :update
  def register
    xml = {
      walletIp: self.walletIP,
      wallet: self.id.to_s,
      clientMail: self.user.email,
      clientFirstName: self.user.first_name,
      clientLastName: self.user.last_name
    }
    response = LemonWay.post "RegisterWallet", xml
    result = Hash.from_xml(Hash.from_xml(response)["Envelope"]["Body"]["RegisterWalletResponse"]["RegisterWalletResult"])
    if result["WALLET"].nil?
      #TODO handle error if email already exists in lemonway but not in the database (useful for the seed...)
      xml = {
        walletIp: self.walletIP,
        wallet: self.id.to_s,
        newEmail: self.user.email,
        newFirstName: self.user.first_name,
        newLastName: self.user.last_name,
        newIp: self.walletIP
      }
      response = LemonWay.post "UpdateWalletDetails", xml
      result = Hash.from_xml(Hash.from_xml(response)["Envelope"]["Body"]["UpdateWalletDetailsResponse"]["UpdateWalletDetailsResult"])
      if !result["WALLET"].nil?
        lwid = result["WALLET"]["LWID"]
        self.LWID = lwid.to_i
        self.save
      end    
    else
      lwid = result["WALLET"]["LWID"]
      self.LWID = lwid.to_i
      self.save
    end
  end

  def update
    xml = {
      walletIp: self.walletIP,
      wallet: self.id.to_s,
      newEmail: self.user.email,
      newFirstName: self.user.first_name,
      newLastName: self.user.last_name,
      newIp: self.walletIP
    }
    response = LemonWay.post "UpdateWalletDetails", xml
    result = Hash.from_xml(Hash.from_xml(response)["Envelope"]["Body"]["UpdateWalletDetailsResponse"]["UpdateWalletDetailsResult"])
    if !result["WALLET"].nil?
      lwid = result["WALLET"]["LWID"]
    end
  end
end
