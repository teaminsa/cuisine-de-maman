class CommandDish < ActiveRecord::Base
  belongs_to :dish
  belongs_to :command
  has_one :dish_rating
  validates_numericality_of :amount, minimum: 1
end
