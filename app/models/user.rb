class User < ActiveRecord::Base

  after_update :update

  validates :email, presence: true, uniqueness: true, email: true
  validates_presence_of :first_name, :last_name, :telephone

  has_many :dishes, foreign_key: "user_id", class_name: "Dish"
  has_many :commands, foreign_key: "user_id"
  has_many :orders, class_name: "Command", foreign_key: "cook_id"

  has_one :session, foreign_key: "user_id"
  has_one :location, foreign_key: "user_id"

  has_many :availabilities, class_name: "Availability", foreign_key: "user_id"

  has_many :user_pictures

  belongs_to :main_picture, class_name: "UserPicture"
  
  has_many :user_ratings
  has_many :ratings, class_name: "Rating", foreign_key: "rater_id"

  has_one :wallet
  has_one :facebook, class_name: :UserFacebook
  #update the wallet infos when user changes
  def update
    self.wallet.save
  end

  def availability_at(datetime)
    self.availabilities.each do |availability|
      if availability.start <= datetime and datetime < availability.end
        return availability
      end
    end
    nil
  end

end
