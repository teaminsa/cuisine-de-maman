class UserFacebook < ActiveRecord::Base
  belongs_to :user
  validates_presence_of :facebook_user_id
end
