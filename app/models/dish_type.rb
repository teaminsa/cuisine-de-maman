class DishType < ActiveRecord::Base
  
  has_many :dishes, class_name: "Dish", foreign_key: "dish_type_id"
  
end
