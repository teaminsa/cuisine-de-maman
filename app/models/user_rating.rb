class UserRating < ActiveRecord::Base
  belongs_to :user
  belongs_to :rating
  has_one :command
end
