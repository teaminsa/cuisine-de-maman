class DishRating < ActiveRecord::Base
  belongs_to :rating
  belongs_to :command_dish
end
