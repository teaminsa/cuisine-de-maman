class Availability < ActiveRecord::Base
  belongs_to :user, foreign_key: "user_id"
  has_many :dish_availabilities, class_name: "DishAvailability", foreign_key: "availability_id"
  has_many :dishes, through: :dish_availabilities
  has_many :commands

  
end
