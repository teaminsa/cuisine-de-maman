require 'bigdecimal'
include ActionView::Helpers::NumberHelper

class CommandsController < ApplicationController
  before_action :authenticate, only: [:create, :update, :destroy, :paymentUrl, :checkPaymentStatus, :delivered]
  before_action :set_command, only: [:show, :update, :destroy, :paymentUrl, :checkPaymentStatus, :delivered]

  # GET users/1/commands
  # GET users/1/commands.json
  def index
    @commands = Command.where("user_id = ?", params[:user_id])
  end

  # GET users/1/commands/1
  # GET users/1/commands/1.json
  def show
  end

  # POST users/1/commands
  # POST users/1/commands.json
  def create
    error = nil
    user = User.find(params[:user_id])
    cook = User.find(command_params[:cook_id])
    delivery_datetime = Time.at(command_params[:delivery_datetime].to_i)
    availability = cook.availability_at(delivery_datetime)
    @command = Command.new(delivery_datetime: delivery_datetime, user: user, cook: cook, price: BigDecimal.new("0"))
    @command.availability = availability
    if command_params[:dishes].instance_of? Array and command_params[:dishes].length > 0
      command_params[:dishes].each do |command_dish|
        amount = command_dish[:amount].to_i
        if amount > 0
          dish = Dish.find(command_dish[:id])
          if dish.user.id == cook.id
            @command.command_dishes << CommandDish.new(dish: dish, command: @command, amount: amount)
            @command.price += dish.price * amount
          else
            error = { :error => "DISH_DOESNT_BELONG_TO_COOK"}
          end
        end
      end
    else
      error = { :error => "COMMAND_WITH_NO_DISHES"}
    end
    if error.nil?
      @command.command_status = CommandStatus.find_by_status "PENDING"
      respond_to do |format|
        if @command.save
          format.json { render :show, status: :created }
        else
          format.json { render json: @command.errors, status: :unprocessable_entity }
        end
      end
    else
      respond_to do |format|
        format.json { render :json => error.to_json, :status => 401 }
      end
    end
  end

  # GET users/1/commands/1/paymentUrl
  def paymentUrl
    # TODO only accessible by the payment provider
    if @command.command_status.status == "PENDING"
      #generate the URL to redirect the user to the payment      
      if !@command.payment.nil?
        #suppress the old payment if it exists
        payment = @command.payment
        payment.destroy        
      end
      payment = Payment.create()
      xml = {
        walletIp: request.remote_ip,
        wallet: @command.cook.wallet.id.to_s,
        amountTot: number_with_precision(@command.price, precision: 2).to_s,
        wkToken: payment.id.to_s,
        returnUrl: APP_CONFIG["site_url"] + "checkout/" + @command.id.to_s + "/succeeded",
        cancelUrl: APP_CONFIG["site_url"] + "checkout/" + @command.id.to_s + "/canceled",
        errorUrl: APP_CONFIG["site_url"] + "checkout/" + @command.id.to_s + "/failed",
        autocomission: '0',
        amountCom: '2.00'
      }
      response = LemonWay.post "MoneyInWebInit", xml
      result = Hash.from_xml(response)["Envelope"]["Body"]["MoneyInWebInitResponse"]["MoneyInWebInitResult"]      
      token = Hash.from_xml(result)["MONEYINWEB"]["TOKEN"]           

      @command.payment = payment
      @command.save

      urlResult = APP_CONFIG["lemon_way_webkit"] + "?moneyInToken=" + token
      result = { url: urlResult }
      respond_to do |format|
        format.json { render :json => result.to_json, status: :ok }
      end
    else
      error = { :error => "ERROR_COMMAND_NOT_PENDING"}
      respond_to do |format|
        format.json { render :json => error.to_json, :status => 401 }
      end
    end
  end
 
  def checkPaymentStatus
    if !@command.payment.nil?
      #The command has at least one payment url generated
      xml = {
        walletIp: request.remote_ip,
        transactionMerchantToken: @command.payment.id
      }
      response = LemonWay.post "GetMoneyInTransDetails", xml
      result = Hash.from_xml(response)["Envelope"]["Body"]["GetMoneyInTransDetailsResponse"]["GetMoneyInTransDetailsResult"]
      puts result
      hpays = Hash.from_xml(result)["TRANS"]["HPAY"]  
      status = 0
      money_in = 0
      if hpays.is_a?(Hash)
        #we only have one money-in, normal behaviour
        if hpays["STATUS"].to_i == 16
          status = 16
          money_in = hpays["ID"].to_i
        end
      else
        #There are many moneyIn into the given transaction (shouldn't happen except in PROD, only for DEV and TEST)
        for hpay in hpays
          puts hpay
          if hpay["STATUS"].to_i == 16
            status = 16
            money_in = hpay["ID"].to_i
          end
        end
      end
      if (status == 16) and (@command.command_status == CommandStatus.find_by_status("PENDING"))
        payment = @command.payment
        @command.command_status = CommandStatus.find_by_status "PAID"      
        @command.save
        payment.transactionId = money_in
        payment.save
      end
    end
    respond_to do |format|
      format.json { render :show, status: :ok}
    end
  end
  
  #PUT users/1/commands/1/delivered
  def delivered
    if @command.command_status.status == "ACCEPTED" && Time.now > @command.availability.start
      @command.command_status = CommandStatus.find_by_status "DELIVERED"
      if @command.save
        respond_to do |format|
          format.json { render :show, status: :ok }
        end
      else
        respond_to do |format|
          format.json { render json: @order.errors, status: :unprocessable_entity }
        end
      end
    else
      error = { :error => "ERROR_COMMAND_NOT_ACCEPTED_OR_DELIVERY_DATE_NOT_REACHED"}
      respond_to do |format|
        format.json { render :json => error.to_json, :status => :forbidden }
      end
    end
  end


  # DELETE users/1/commands/1
  # DELETE users/1/commands/1.json
  def destroy
    @command.destroy
    respond_to do |format|
      format.json { head :no_content }
    end
  end

 # POST /commands/reschedule
  def reschedule
    @dishes = []
    @availability = Availability.find(params[:availability_id])
    cook = User.find(params[:cook_id])   
    if params[:dishes].instance_of? Array
      params[:dishes].each do |param_dish|
        dish = Dish.find param_dish[:id]
        if @availability.dishes.include? dish
          amount = param_dish[:amount].to_i
          # TODO check if there are dishes left
          @dishes << {
            id: dish.id,
            amount: amount
          }
        end
      end
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_command
    @command = Command.where("user_id = ? and id = ?", params[:user_id], params[:id]).first
    if @command.nil?
      respond_to do |format|
        format.json { head :not_found }
      end
    end
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def command_params
    params.require(:command).permit(:cook_id, :delivery_datetime, dishes: [:id, :amount])
  end
end
