class RatingsController < ApplicationController
  before_action :authenticate, only: [:create, :update, :destroy, :rate_cook, :rate_client, :rate_dish]
  before_action :set_user_rating, only: [:show, :edit, :update, :destroy]

  # GET /user_ratings/1
  # GET /user_ratings/1.json
  def show
  end
  
  # PATCH/PUT /user_ratings/1
  # PATCH/PUT /user_ratings/1.json
  def update
    respond_to do |format|
      if @user_rating.update(picture_params)
        format.html { redirect_to @user_rating, notice: 'Rating was successfully updated.' }
        format.json { render :show, status: :ok, location: @user_rating }
      else
        format.html { render :edit }
        format.json { render json: @user_rating.errors, status: :unprocessable_entity }
      end
    end
  end
  
  #POST /users/1/commands/1/rate
  def rate_cook    
    command = Command.find(params[:id])
    if !command.cook_rating.nil?
       respond_to do |format|
        format.json { render json: { error: "ALREADY_RATED" }, status: :forbidden }
      end
    elsif command.command_status != CommandStatus.find_by_status("DELIVERED")
      respond_to do |format|
        format.json { render json: { error: "STATUS_NOT_DELIVERED" }, status: :forbidden }
      end 
    else
      rater = User.find(params[:user_id])
      user = command.cook
      rating = Rating.new(comments: rating_params[:comments], value: rating_params[:value].to_i, rater: rater)
      @user_rating = UserRating.new(rating: rating, user: user)
      @user_rating.save
      command.cook_rating = @user_rating
      command.save
      respond_to do |format|
        if @user_rating.save
          format.html { redirect_to @user_rating, notice: 'Rating was successfully updated.' }
          format.json { render template: "user_ratings/show", status: :ok}
        else
          format.html { render :edit }
          format.json { render json: @user_rating.errors, status: :unprocessable_entity }
        end
      end
    end
  end
  
  #POST /users/1/orders/1/rate
  def rate_client
    order = Command.find(params[:order_id])
    if order.user_rating.nil?
       respond_to do |format|
        format.json { render json: { error: "ALREADY_RATED" }, status: :forbidden }
      end
    elsif order.command_status != CommandStatus.find_by_status("DELIVERED")
      respond_to do |format|
        format.json { render json: { error: "STATUS_NOT_DELIVERED" }, status: :forbidden }
      end 
    else
      rater = User.find(params[:user_id])
      user = order.user
      rating = Rating.new(comments: rating_params[:comments], value: rating_params[:value], rater: rater)
      @user_rating = UserRating.new(rating: rating, user: user)
      @user_rating.save
      order.cook_rating = @user_rating
      order.save
      respond_to do |format|
        if @user_rating.save
          format.html { redirect_to @user_rating, notice: 'Rating was successfully updated.' }
          format.json { render template: "user_ratings/show", status: :ok}
        else
          format.html { render :edit }
          format.json { render json: @user_rating.errors, status: :unprocessable_entity }
        end
      end
    end
  end

  # POST /users/1/commands/1/dishes/1/rate
  def rate_dish
    command = Command.find(params[:id])
    dish = Dish.find(params[:dish_id])
    if !command.command_dishes.where(dish: dish).first.dish_rating.nil?
     respond_to do |format|
      format.json { render json: { error: "ALREADY_RATED" }, status: :forbidden }
    end
    elsif command.command_status != CommandStatus.find_by_status("DELIVERED")
      respond_to do |format|
        format.json { render json: { error: "STATUS_NOT_DELIVERED" }, status: :forbidden }
      end 
    else      
      command_dish = command.command_dishes.where(dish: dish).first
      rater = User.find(params[:user_id])
      rating = Rating.new(comments: rating_params[:comments], value: rating_params[:value], rater: rater)
      @dish_rating = DishRating.new(rating: rating, command_dish: command_dish)
      respond_to do |format|
        if @dish_rating.save
          format.html { redirect_to @dish_rating, notice: 'Rating was successfully updated.' }
          format.json { render template: "dish_ratings/show", status: :ok}
        else
          format.html { render :edit }
          format.json { render json: @dish_rating.errors, status: :unprocessable_entity }
        end
      end
    end
  end

  # DELETE /pictures/1
  # DELETE /pictures/1.json
  def destroy
    @user_rating.destroy
    respond_to do |format|
      format.html { redirect_to pictures_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_rating
      @rating = Rating.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def rating_params
      permitted_params = params.require(:rating).permit(:value, :comments)
      permitted_params
    end
end
