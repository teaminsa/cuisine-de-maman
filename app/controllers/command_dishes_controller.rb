class CommandDishesController < ApplicationController
  before_action :set_command_dish, only: [:show, :edit, :update, :destroy]

  # GET /command_dishes
  # GET /command_dishes.json
  def index
    @command_dishes = CommandDish.all
  end

  # GET /command_dishes/1
  # GET /command_dishes/1.json
  def show
  end

  # GET /command_dishes/new
  def new
    @command_dish = CommandDish.new
  end

  # GET /command_dishes/1/edit
  def edit
  end

  # POST /command_dishes
  # POST /command_dishes.json
  def create
    @command_dish = CommandDish.new(command_dish_params)

    respond_to do |format|
      if @command_dish.save
        format.html { redirect_to @command_dish, notice: 'Command dish was successfully created.' }
        format.json { render :show, status: :created, location: @command_dish }
      else
        format.html { render :new }
        format.json { render json: @command_dish.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /command_dishes/1
  # PATCH/PUT /command_dishes/1.json
  def update
    respond_to do |format|
      if @command_dish.update(command_dish_params)
        format.html { redirect_to @command_dish, notice: 'Command dish was successfully updated.' }
        format.json { render :show, status: :ok, location: @command_dish }
      else
        format.html { render :edit }
        format.json { render json: @command_dish.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /command_dishes/1
  # DELETE /command_dishes/1.json
  def destroy
    @command_dish.destroy
    respond_to do |format|
      format.html { redirect_to command_dishes_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_command_dish
      @command_dish = Commanddish.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def command_dish_params
      params[:command_dish]
    end
end
