class OrdersController < ApplicationController
  before_action :authenticate
  before_action :set_order, only: [:accept, :decline, :destroy, :gift, :show, :update]

  # GET users/1/orders
  # GET users/1/orders.json
  def index
    @orders = Command.where cook_id: params[:user_id]
  end

  # GET users/1/orders/1
  # GET users/1/orders/1.json
  def show
  end

  # PUT users/1/orders/1/accept
  def accept
    if @order.command_status == CommandStatus.find_by_status("PAID")
      #accept the payment on lemonway
      xml = {
        walletIp: request.remote_ip,
        transactionId: @order.payment.transactionId
      }
      response = LemonWay.post "MoneyInValidate", xml
      result = Hash.from_xml(response)["Envelope"]["Body"]["MoneyInValidateResponse"]["MoneyInValidateResult"]      
      if !Hash.from_xml(result)["MONEYIN"].nil?
        #the transaction has been found and we accept the command
        status = Hash.from_xml(result)["MONEYIN"]["HPAY"]["STATUS"]        
        if status.to_i == 3
          #Status always will be 3 --> accepted
          @order.command_status = CommandStatus.find_by_status("ACCEPTED")
          if @order.save
            respond_to do |format|
              format.json { render :show, status: :ok }
            end
          else
            respond_to do |format|
              format.json { render json: @order.errors, status: :unprocessable_entity }
            end
          end
        end
      else
        #happens when the command already has been validated or if the database and lemonway are not synchronized (moneyIn IDs)
        error = { :error => "ERROR_INVALID_TRANSACTION"}
        respond_to do |format|
          format.json { render :json => error.to_json, :status => :forbidden }
        end     
      end
    else
      error = { :error => "ERROR_COMMAND_NOT_PAID"}
      respond_to do |format|
        format.json { render :json => error.to_json, :status => :forbidden }
      end
    end
  end

  # PUT users/1/orders/1/decline
  def decline
    if @order.command_status.status == "PAID"
      @order.command_status = CommandStatus.find_by_status "DECLINED"
      if @order.save
        respond_to do |format|
          format.json { render :show, status: :ok }
        end
      else
        respond_to do |format|
          format.json { render json: @order.errors, status: :unprocessable_entity }
        end
      end
    else
      error = { :error => "ERROR_COMMAND_NOT_PAID"}
      respond_to do |format|
        format.json { render :json => error.to_json, :status => :forbidden }
      end
    end
  end

  # PUT users/1/orders/1/gift
  def gift
    if @order.command_status.status == "PENDING"
      if @order.update_attribute :command_status, CommandStatus.find_by_status("PAID")
        respond_to do |format|
          format.json { render :show, status: :ok }
        end
      else
        respond_to do |format|
          format.json { render json: @order.errors, status: :unprocessable_entity }
        end
      end
    else
      error = { :error => "ERROR_COMMAND_NOT_PENDING"}
      respond_to do |format|
        format.json { render :json => error.to_json, :status => :forbidden }
      end
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_order
    @order = Command.find params[:order_id]
      
    if @order.nil?
      respond_to do |format|
        format.json { head :not_found }
      end
    end
    
    # check that the user is the cook
    if not @session.user.eql? @order.cook
      error = { :error => "ERROR_NOT_COOK"}
      respond_to do |format|
        format.json { render :json => error.to_json, :status => :unauthorized }
      end
    end
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def order_params
    params.require(:order).permit(:price, :delivery_time, cook: [:id], command_dishes: [:dish_id, :nb_portions])
  end
end
