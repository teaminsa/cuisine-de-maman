class LocationsController < ApplicationController

  before_action :authenticate, only: [ :create, :edit, :update, :destroy]
  before_action :set_location, only: [:show, :edit, :update, :destroy, :index]
  
  # GET /users/1/locations
  # GET /users/1/locations.json
  def index
  end

  # GET /users/1/locations/1
  # GET /users/1/locations/1.json
  #ne sert à rien...
  def show
  end

  # GET /users/1/locations/new  
  def new
    @location = Location.new
  end

  # GET /users/1/locations/edit
  def edit
  end

  # POST /users/1/locations
  # POST /users/1/locations.json
  def create
    if @location.nil?
      @location = Location.new(location_params)
      @user = User.find(params[:user_id])
      @location.user = @user
      respond_to do |format|
        if @location.save
          format.html { redirect_to @location, notice: 'Location was successfully created.' }
          format.json { render :show, status: :created, location: @location }
        else
          format.html { render :new }
          format.json { render json: @location.errors, status: :unprocessable_entity }
        end
      end
    else
      respond_to do |format|
        if @location.update(location_params)
          format.html { redirect_to @location, notice: 'Location was successfully updated.' }
          format.json { render :show, status: :ok, location: @location }
        else
          format.html { render :edit }
          format.json { render json: @location.errors, status: :unprocessable_entity }
        end
      end         
    end
  end

  # PATCH/PUT /users/1/locations/1
  # PATCH/PUT /users/1/locations/1.json
  def update    
    if @location.nil?
      @location = Location.new(location_params)
      @user = User.find(params[:user_id])
      @location.user = @user
      respond_to do |format|
        if @location.save
          format.html { redirect_to @location, notice: 'Location was successfully created.' }
          format.json { render :show, status: :created, location: @location }
        else
          format.html { render :new }
          format.json { render json: @location.errors, status: :unprocessable_entity }
        end
      end
    else
      respond_to do |format|
        if @location.update(location_params)
          format.html { redirect_to @location, notice: 'Location was successfully updated.' }
          format.json { render :show, status: :ok, location: @location }
        else
          format.html { render :edit }
          format.json { render json: @location.errors, status: :unprocessable_entity }
        end
      end         
    end
  end

  # DELETE /users/1/locations/1
  # DELETE /users/1/locations/1.json
  def destroy
    @location.destroy
    respond_to do |format|
      format.html { redirect_to locations_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_location
      @location = Location.where("user_id = ?", params[:user_id]).first
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def location_params
      params.require(:location).permit(:latitude, :longitude, :address, :informations, :city)
    end
end
