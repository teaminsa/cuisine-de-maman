class DishesController < ApplicationController
  before_action :authenticate, only: [:create, :update, :destroy, :add_picture]
  before_action :set_dish, only: [:show, :update, :destroy, :add_picture]

  # GET /dishes/search
  def search
    latitude_north = params[:latitude_north].to_f
    latitude_south = params[:latitude_south].to_f
    longitude_east = params[:longitude_east].to_f
    longitude_west = params[:longitude_west].to_f
    if !params[:interval_start].nil? and !params[:interval_end].nil?
      interval_start = Time.at params[:interval_start].to_i
      interval_end = Time.at params[:interval_end].to_i
      has_interval = true
    else
      has_interval = false
    end
    if !latitude_north.nil? and !longitude_west.nil? and !latitude_south.nil? and !longitude_east.nil?
      @results = []
      #iterate on the users because whe there are the ones having the location, moreover it will do less loops
      users = User.all
      for user in users
        location =  user.location
        #check that the user has a location and that he has some dishes
        if !location.nil? and user.dishes.count > 0        
          if latitude_south < location.latitude and location.latitude < latitude_north and longitude_west < location.longitude and location.longitude < longitude_east
            for dish in user.dishes
              #check the availability of the dish
              if has_interval
                availability = dish.available_between interval_start, interval_end
                if availability
                  @results << {
                    availability: availability,
                    dish: dish
                  }
                end
              else
                if dish.has_coming_availabilities                           
                  @results << {
                    availability: dish.first_availability,
                    dish: dish
                  }
                end
              end
            end
          end
        end
      end
    else
      respond_to do |format|
        format.json { render json: { error: "WRONG_PARAMETER" }, status: :forbidden }
      end
    end
  end

  # GET users/1/dishes
  # GET users/1/dishes.json
  def index
    if !params[:user_id].nil?
      @dishes = Dish.where("user_id = ?", params[:user_id])
    else
      @dishes = Dish.all
    end
  end

  # GET users/1/dishes/1
  # GET users/1/dishes/1.json
  def show
  end

  # POST users/1/dishes
  # POST users/1/dishes.json
  def create
    #We get the user from the json
    user = User.find(params[:user_id])
    #We create the dish, excluding the user from the json to avoid a casting error (int -> User)
    @dish = Dish.new(dish_params)
    #Association of the dish and the user
    @dish.user = user
    respond_to do |format|
      if @dish.save
        format.json { render :show, status: :created, location: @dish }
      else
        format.json { render json: @dish.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT users/1/dishes/1
  # PATCH/PUT users/1/dishes/1.json
  def update
    respond_to do |format|
      if @dish.update(dish_params)
        format.json { render :show, status: :ok, location: @dish }
      else
        format.json { render json: @dish.errors, status: :unprocessable_entity }
      end
    end
  end

  #POST /users/1/dishes/1/add_picture
  def add_picture
    @picture = Picture.new(file: params[:file])
    @dishpicture = DishPicture.new(dish: @dish, picture: @picture)
    if @dish.main_picture.nil?
      @dish.main_picture = @dishpicture
    end
    respond_to do |format|
      if (@picture.save && @dish.save)
        format.json { render :show, status: :created, location: @picture }
      else
        format.json { render json: @picture.errors, status: :unprocessable_entity }
      end
    end
  end
  
  #GET /dishes/cuisine_keywords
  def cuisine_keywords
    @cuisine_keywords = CuisineKeyword.all
    respond_to do |format|
      format.json { render template: "cuisine_keywords/index", status: :ok}
    end
  end

  # DELETE users/1/dishes/1
  # DELETE users/1/dishes/1.json
  def destroy
    @dish.destroy
    respond_to do |format|
      format.json { head :no_content }
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_dish
    @dish = Dish.find params[:id]
    if @dish.nil?
      respond_to do |format|
        format.json { render json: { error: "ID_DOESNT_EXIST" }, status: :not_found }
      end
    end
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def dish_params
    permitted_params = params.require(:dish).permit(:description, :type, :name, :price, cuisine_keywords: [:id])
    if permitted_params[:type]
      permitted_params[:type] = DishType.find permitted_params[:type]
    end
    if permitted_params[:cuisine_keywords]
      cuisine_keywords = []
      for keyword in permitted_params[:cuisine_keywords]
        cuisine_keyword = CuisineKeyword.find(keyword[:id])
        cuisine_keywords << cuisine_keyword
      end
      permitted_params[:cuisine_keywords] = cuisine_keywords
    end
    permitted_params
  end
end
