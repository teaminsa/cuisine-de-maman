require "SessionManager.rb"

class ApplicationController < ActionController::Base
  # disable CSRF token since we authenticate with a session token
  protect_from_forgery with: :null_session

  rescue_from ActionController::ParameterMissing, with: :parameter_missing

  respond_to :json

  protected
  def authenticate
    @session = SessionManager.getSession(params[:user_id], params[:token])
    if @session.nil?
      respond_to do |format|
        format.json { render json: { error: "AUTHENTICATION_REQUIRED" }, status: :unauthorized }
      end
    end
  end

  private

  def parameter_missing
    respond_to do |format|
      format.json { render json: { error: "PARAMETER_MISSING" }, status: :unauthorized }
    end
  end
end
