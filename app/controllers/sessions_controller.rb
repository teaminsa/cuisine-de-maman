require "SessionManager.rb"

class SessionsController < ApplicationController
  before_action :authenticate, only: [:destroy, :validate]

  # POST /session
  def connect
    if !params[:facebook].nil?
      @session = SessionManager.connectionFacebook params[:facebook]
    else
      @session = SessionManager.connection params[:email], params[:password]
    end

    if !@session.nil?
      render :show, status: :created
    else
      respond_to do |format|
        format.json { render json: { error: "WRONG_CREDENTIALS" }, status: :unauthorized }
      end
    end
  end

  # GET /sessions/:id
  def validate
    render :show
  end

  # DELETE /sessions/:id
  def destroy
    @session.destroy
    respond_to do |format|
      format.json { head :no_content }
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_session
    @session = Session.find params[:id]
  end
end
