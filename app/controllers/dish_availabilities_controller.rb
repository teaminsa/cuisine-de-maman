class DishesAvailabilitiesController < ApplicationController
  before_action :set_dish_availability, only: [:show, :edit, :update, :destroy]

  # GET /dish_availabilities
  # GET /dish_availabilities.json
  def index
    @dish_availabilities = DishAvailability.all
  end

  # GET /dish_availabilities/1
  # GET /dish_availabilities/1.json
  def show
  end

  # GET /dish_availabilities/new
  def new
    @dish_availability = DishAvailability.new
  end

  # GET /dish_availabilities/1/edit
  def edit
  end

  # POST /dish_availabilities
  # POST /dish_availabilities.json
  def create
    @dish_availability = DishAvailability.new(dish_availability_params)
    respond_to do |format|
      if @dish_availability.save
        format.html { redirect_to @dish_availability, notice: 'dish availability was successfully created.' }
        format.json { render :show, status: :created, location: @dish_availability }
      else
        format.html { render :new }
        format.json { render json: @dish_availability.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /dish_availabilities/1
  # PATCH/PUT /dish_availabilities/1.json
  def update
    respond_to do |format|
      if @dish_availability.update(dish_availability_params)
        format.html { redirect_to @dish_availability, notice: 'dish availability was successfully updated.' }
        format.json { render :show, status: :ok, location: @dish_availability }
      else
        format.html { render :edit }
        format.json { render json: @dish_availability.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /dish_availabilities/1
  # DELETE /dish_availabilities/1.json
  def destroy
    @dish_availability.destroy
    respond_to do |format|
      format.html { redirect_to dish_availabilities_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_dish_availability
      @dish_availability = DishAvailability.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def dish_availability_params
      params[:dish_availability]
    end
end
