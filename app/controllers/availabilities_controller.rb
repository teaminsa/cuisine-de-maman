require "SessionManager.rb"

class AvailabilitiesController < ApplicationController
  before_action :authenticate, only: [:add_dish, :create, :destroy, :update, :remove_dish]
  before_action :set_availability, only: [:add_dish, :destroy, :show, :update, :remove_dish]

  # GET /users/1/availabilities
  # GET /users/1//availabilities.json
  def index
    @availabilities = Availability.where("user_id = ?", params[:user_id])
  end

  # GET /availabilities/1
  # GET /availabilities/1.json
  def show
  end

  # POST /availabilities
  # POST /availabilities.json
  def create
    @availability = Availability.new(availability_params)
    #the only user who can add an availability is the owner himself
    @user = User.find(params[:user_id])
    @availability.user = @user
    respond_to do |format|
      if @availability.save
        format.json { render :show, status: :created, location: @availability }
      else
        format.json { render json: @availability.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /availabilities/1
  # PATCH/PUT /availabilities/1.json
  def update
    respond_to do |format|
      if @availability.update(availability_params)
        format.json { render :show, status: :ok, location: @availability }
      else
        format.json { render json: @availability.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /availabilities/1
  # DELETE /availabilities/1.json
  def destroy
    @availability.destroy
    respond_to do |format|
      format.json { head :no_content }
    end
  end

  def add_dish
    dish = Dish.find(params[:dish_id])
    if dish.nil?
      head :not_found
    else
      unless @availability.dishes.include?(dish)
        @availability.dishes << dish
      end
      head :no_content
    end
  end

  def remove_dish
    dish = DishAvailability.find_by_availability_id_and_dish_id(@availability.id, params[:dish_id])
    if dish.nil?
      head :not_found
    else
      dish.destroy
      head :no_content
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_availability
    @availability = Availability.find params[:id]
    if @availability.nil?
      respond_to do |format|
        format.json { head :not_found }
      end
    end
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def availability_params
    params.require(:availability).permit(:end, :start)
  end
end
