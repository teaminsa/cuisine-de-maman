require "EncryptionManager.rb"
require "SessionManager.rb"

class UsersController < ApplicationController
  before_action :authenticate, only: [:update, :destroy]
  before_action :set_user, only: [:show, :update, :destroy]

  # GET /users/1
  # GET /users/1.json
  def show
  end
  
    # GET /users
  # GET /users.json
  def index
    @users = User.all
  end


  # POST /users
  # POST /users.json
  def create
    @user = User.new(user_params)

    if params[:facebook]
      @user.facebook = UserFacebook.new(facebook_user_id: params[:facebook])

    elsif @user.password.nil?
      return respond_to do |format|
        format.json { render json: { error: "PASSWORD_OR_FACEBOOK_REQUIRED" }, status: :forbidden }
      end
    end

    @user.wallet =  Wallet.new(walletIP: request.remote_ip)

    respond_to do |format|
      if @user.save
        session = SessionManager.newSession(@user)
        @user.session = session
        @user.save
        format.json { render :creation, status: :created, location: @user }
      else
        if !@user.facebook.nil?
          @user.facebook.destroy()
        end
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /users/1
  # PATCH/PUT /users/1.json
  def update
    @user = User.find(params[:user_id])
    respond_to do |format|
      if @user.update(user_new_params)
        format.json { render :show, status: :ok, location: @user }
      else
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /users/1
  # DELETE /users/1.json
  def destroy
    @user = User.find(params[:user_id])
    @user.destroy
    respond_to do |format|
      format.json { head :no_content }
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_user
    @user = User.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def user_params
    permitted_params = params.require(:user).permit(:email, :first_name, :last_name, :password, :telephone)
    if permitted_params[:password]
      permitted_params[:salt] = EncryptionManager.salt
      permitted_params[:password] = EncryptionManager.hashPassword(permitted_params[:password], permitted_params[:salt])
    end
    permitted_params
  end

  def facebook_params
    params.require(:user).permit(:facebook)
  end
end
