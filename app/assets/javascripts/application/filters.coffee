angular.module 'PlatMaison'

# I18ize dates
.filter 'idate', ->
  (date) ->
    return '' if not date
    date.format 'LL'

# I18ize times
.filter 'itime', ->
  (date) ->
    return '' if not date
    date.format 'LT'
