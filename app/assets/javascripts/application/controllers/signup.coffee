angular.module 'PlatMaison'

.controller 'Signup', ['$scope', 'api', 'session', 'util', ($scope, api, session, util) ->
  $scope.data = {}
  
  util.initForm()
  
  $scope.validateEmail = ->
    group = util.resetGroup '#email'
    if /^.+@.+\..+$/.test $scope.data.email
      return util.groupSuccess group
    util.groupError group
    
  $scope.validatePassword = ->
    group = util.resetGroup '#password'
    if $scope.data.password?.length > 8
      return util.groupSuccess group
    if $scope.data.password?.length > 0
      return util.groupWarning group
    util.groupError group
    
  $scope.validateFirstName = ->
    group = util.resetGroup '#first_name'
    if $scope.data.first_name?.length > 1
      return util.groupSuccess group
    util.groupError group
    
  $scope.validateLastName = ->
    group = util.resetGroup '#last_name'
    if $scope.data.last_name?.length > 1
      return util.groupSuccess group
    util.groupError group
    
  $scope.validateTelephone = ->
    group = util.resetGroup '#telephone'
    if /^\+?[ \-\.\/\d]+$/.test $scope.data.telephone
      return util.groupSuccess group
    util.groupError group
    
  $scope.facebookUsed = false
  $scope.passwordUsed = true
  
  $scope.usePassword = ->
    $scope.passwordUsed = true
  
  validateFields = ->
    $scope.validateEmail() and $scope.validateFirstName() and $scope.validateLastName() and $scope.validateTelephone() and
      (!$scope.passwordUsed or $scope.validatePassword())
    
  $scope.connectWithFacebook = ->
    loginFacebook (response) ->
      if response.status == 'connected'
        $scope.facebookUsed = true
        $scope.passwordUsed = false
        FB.api '/me', (me) ->
          FB.api '/me/picture?redirect=false&type=normal', (picture) ->
            $scope.$apply ->
              $scope.data.facebook = response.authResponse.userID
              $scope.data.facebookPicture = picture.data.url
              $scope.data.email = me.email
              $scope.data.first_name = me.first_name
              $scope.data.last_name = me.last_name
              validateFields()
      
  $scope.submit = ->
    if validateFields()
      if !$scope.passwordUsed
        delete $scope.data.password
      api.users.create $scope.data
      .success (data) ->
        data.session.user = data.user
        session.handleResponse data.session
      # .error ->

  return
]
