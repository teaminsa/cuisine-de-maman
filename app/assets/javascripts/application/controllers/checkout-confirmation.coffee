angular.module 'PlatMaison'

.controller 'CheckoutConfirmation', ['$http', '$routeParams', '$scope', 'api', 'session', 'util', ($http, $routeParams, $scope, api, session, util) ->
  $scope.view = $routeParams.view
  
  if $scope.view is 'succeeded'
    api.users.commands.checkPaymentStatus $routeParams.commandId
    .success (data) ->
      command = data.command
      util.processAvailability command.availability
      
      switch command.status
        when 'PAID'
          $scope.view = 'confirmed'
          $scope.command = command
        else
          $scope.view = 'error'
]
