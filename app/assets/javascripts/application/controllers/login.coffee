angular.module 'PlatMaison'

.controller 'Login', ['$scope', 'api', 'session', 'util', ($scope, api, session, util) ->
  util.initForm()
  
  errorCallback = (data) ->
    if data?.error == 'WRONG_CREDENTIALS'
      $scope.wrongCredentials = true
      
  $scope.logInWithFacebook = ->
    $scope.wrongCredentials = false
    session.logInWithFacebook errorCallback

  $scope.submit = ->
    $scope.wrongCredentials = false
    session.logIn $scope.email or $('#email').val(), $scope.password or $('#password').val(), errorCallback
  
  return
]
