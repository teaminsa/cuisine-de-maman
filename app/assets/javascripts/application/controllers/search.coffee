angular.module 'PlatMaison'

.controller 'Search', ['$location', '$scope', 'api', 'search', 'url', ($location, $scope, api, search, url) ->
  $scope.search = search
  $scope.url = url
  
  $map = $ '#map'
  map = new google.maps.Map $map.children()[0]
  
  $scope.highlight = (result) ->
    result.marker.setIcon '/assets/marker-highlighted.png'
    
  $scope.unhighlight = (result) ->
    result.marker.setIcon '/assets/marker.png'
  
  markers = []
  openInfoWindow = null
  update = ->
    api.dishes.search
      amount: search.amount
      interval_start: search.getIntervalStart()
      interval_end: search.getIntervalEnd()
      latitude_north: search.north
      latitude_south: search.south
      longitude_east: search.east
      longitude_west: search.west
    .success (data) ->
      $scope.results = data
      
      m.setMap null for m in markers
      openInfoWindow?.close()
            
      cooks = {}
      markers = []
      data.forEach (result) ->
        cookKey = String result.cook.id
        cook = cooks[cookKey]
        
        if not cook
          marker = new google.maps.Marker
            icon: '/assets/marker.png'
            map: map
            position: new google.maps.LatLng result.cook.location.lat, result.cook.location.lng
          
          $info = $ '<div>'
          
          $a = $ '<a>'
          $info.append $a
          $a.attr 'href', url.dish result
          $a.text result.dish.name
          
          infoWindow = new google.maps.InfoWindow
            content: $info.html()
            
          google.maps.event.addListener marker, 'click', ->
            openInfoWindow?.close()
            infoWindow.open map, marker
            openInfoWindow = infoWindow
          
          markers.push marker
          result.marker = marker
          
          cooks[cookKey] =
            dish: result.dish
            marker: marker
        else
          result.marker = cook.marker
  
  submit = ->
    search.submit()
    update()
  
  $scope.submit = submit
  
  $time = $ '#time'
  
  $date = $ '#date'
  $date.val search.idate
  datepicker = $date.datepicker().data 'datepicker'
  $date.change ->
    search.idate = $date.val()
    datepicker.hide()
    submit()
  
  $location = $ '#location'
  $location.change ->
    map.fitBounds search.getBounds()
    search.location = $location.val()
    submit()
  $location.autocomplete()
  
  idleListener = google.maps.event.addListener map, 'idle', ->
    google.maps.event.removeListener idleListener
    
    map.fitBounds search.getBounds()
    
    update()
    
    google.maps.event.addListener map, 'dragstart', ->
      openInfoWindow?.close()
    
    google.maps.event.addListener map, 'dragend', ->
      bounds = map.getBounds()
      if bounds
        search.setBounds bounds
        search.submit()
        update()
    
    google.maps.event.addListener map, 'zoom_changed', ->
      google.maps.event.trigger this, 'dragend'
]
