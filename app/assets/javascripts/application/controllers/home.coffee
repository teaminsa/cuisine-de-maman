angular.module 'PlatMaison'

.controller 'Home', ['$location', '$scope', 'search', ($location, $scope, search) ->
  $scope.search = search
  
  $time = $ '#time'
  
  $date = $ '#date'
  $date.val search.idate
  datepicker = $date.datepicker().data 'datepicker'
  $date.change ->
    search.idate = $date.val()
    datepicker.hide()
    $time.focus()
  
  $location = $ '#location'
  $location.change ->
    search.location = $location.val()
    if search.location
      $date.focus()
  $location.autocomplete().focus()
  
  $scope.submit = ->
    if not search.submit()
      $location.focus()
    return
  
  return
]
