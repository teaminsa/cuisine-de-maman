angular.module 'PlatMaison'

.controller 'Kitchen', ['$location', '$scope', 'api', 'session', 'url', 'util', ($location, $scope, api, session, url, util) ->
  return if session.notAuthenticated()
  
  $scope.url = url
  
  $('#tabs').tabs
    activate: ->
      $(window).resize()
  
  # Dishes
  
  api.users.dishes.index()
  .success (data) ->
    $scope.dishes = data.dishes
    
    setTimeout ->
      $('#availableDishes > .availableDish').draggable
        containment: 'document'
        helper: 'clone'
    , 0
    
  # Calendar
  
  api.users.availabilities.index()
  .success (data) ->
    availabilities = data.availabilities
    util.processAvailability availability for availability in availabilities
    
    weekCalendar = $ '#weekCalendar'
    monthCalendar = $ '#monthCalendar'
    
    sameDate = (a, b) ->
      a.getFullYear() is b.getFullYear() and a.getMonth() is b.getMonth() and a.getDate() is b.getDate()
        
    cropToDay = (start, end) ->
      if not sameDate start, end
        end.setHours 0
        end.setMinutes 0
        end.setSeconds 0
        end.setMilliseconds 0
    
    overlapping = (availability) ->
      availabilities.some (a) -> a isnt availability and availability.start < a.end and a.start < availability.end
      
    weekCalendar.fullCalendar
      allDayDefault: false
      allDaySlot: false
      defaultView: 'agendaWeek'
      editable: true
      eventDrop: (availability, dayDelta, minuteDelta, allDay, revertFunc, jsEvent, ui, view) ->
        cropToDay availability.start, availability.end
        return revertFunc() if overlapping availability
        saveAvailability availability, revertFunc
      eventClick: (availability) ->
        $scope.$apply ->
          $scope.availability = availability
      eventRender: (availability, element) ->
        element.droppable
          accept: '.availableDish'
          drop: (e, ui) ->
            dishId = ui.draggable.data 'dish-id'
            return if availability.dishes.some (d) -> d.id is dishId
            api.users.availabilities.addDish availability.id, dishId
            .success ->
              availability.dishes.push dish for dish in $scope.dishes when dish.id is dishId
          tolerance: 'pointer'
        return
      eventResize: (availability, dayDelta, minuteDelta, revertFunc, jsEvent, ui, view) ->
        cropToDay availability.start, availability.end
        return revertFunc() if overlapping availability
        saveAvailability availability, revertFunc
      events: availabilities
      firstHour: 10
      header:
        left: 'prev'
        center: 'title'
        right: 'next'
      height: 639
      ignoreTimezone: false
      select: (start, end, allDay) ->
        cropToDay start, end
        
        api.users.availabilities.create
          availability:
            end: end
            start: start
        .success (data) ->
          availability = data.availability
          util.processAvailability availability
          availabilities.push availability
          weekCalendar.fullCalendar 'refetchEvents'
          monthCalendar.fullCalendar 'refetchEvents'
          $scope.availability = availability
      selectable: true
      timezone: 'local'
      
    monthCalendar.fullCalendar
      allDayDefault: false
      allDaySlot: false
      defaultView: 'month'
      disableResizing: true
      editable: true
      eventDrop: (availability, dayDelta, minuteDelta, allDay, revertFunc, jsEvent, ui, view) ->
        return revertFunc() if overlapping availability
        saveAvailability availability, revertFunc
      eventClick: (availability) ->
        $scope.$apply ->
          $scope.availability = availability
      eventRender: (availability, element) ->
        element.droppable
          accept: '.availableDish'
          drop: (e, ui) ->
            dishId = ui.draggable.data 'dish-id'
            return if availability.dishes.some (d) -> d.id is dishId
            api.users.availabilities.addDish availability.id, dishId
            .success ->
              availability.dishes.push dish for dish in $scope.dishes when dish.id is dishId
          tolerance: 'pointer'
        return
      events: availabilities
      firstHour: 10
      header:
        left: 'prev'
        center: 'title'
        right: 'next'
      ignoreTimezone: false
      select: (start, end, allDay) ->
        weekCalendar.fullCalendar 'gotoDate', start
      selectable: true
      timezone: 'local'
      
    saveAvailability = (availability, revertFunc) ->
      util.processAvailability availability
      api.users.availabilities.update availability.id,
        availability:
          dishes: ({ id: dish.id } for dish in availability.dishes)
          end: availability.mEnd.toISOString()
          start: availability.mStart.toISOString()
      .success ->
        weekCalendar.fullCalendar 'refetchEvents'
        monthCalendar.fullCalendar 'refetchEvents'
      .error ->
        revertFunc?()
          
    $scope.removeFromAvailability = (dish) ->
      availability = $scope.availability
      availability.dishes.some (d, index) ->
        if d.id == dish.id 
          api.users.availabilities.removeDish availability.id, dish.id
          .success ->
            availability.dishes.splice index, 1
          true
      
    $scope.removeAvailability = ->
      availability = $scope.availability
      if availability
        api.users.availabilities.destroy availability.id
        .success ->
          index = availabilities.indexOf availability
          if index isnt -1
            availabilities.splice index, 1
            weekCalendar.fullCalendar 'refetchEvents'
            monthCalendar.fullCalendar 'refetchEvents'
            $scope.availability = null
  
  # Orders
  
  api.users.orders.index()
  .success (data) ->
    orders = data.orders
    $scope.orders = orders
    util.processAvailability order.availability for order in orders
    
  $scope.gift = (order) ->
    api.users.orders.gift order.id
    .success (data) ->
      order.status = data.order.status
      
  $scope.accept = (order) ->
    api.users.orders.accept order.id
    .success (data) ->
      order.status = data.order.status
      
  $scope.decline = (order) ->
    api.users.orders.decline order.id
    .success (data) ->
      order.status = data.order.status
    
  now = moment()
  
  $scope.futureOrder = (order) ->
    mStart = order.availability.mStart
    mStart.isSame(now) or mStart.isAfter(now)
    
  $scope.pastOrder = (order) ->
    mStart = order.availability.mStart
    mStart.isBefore(now)
    
  return
]
