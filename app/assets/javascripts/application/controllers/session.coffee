angular.module 'PlatMaison'

.controller 'Session', ['$scope', 'session', ($scope, session) ->
  $scope.$on 'loggedIn', (event, user) ->
    $scope.loggedIn = true
    $scope.user = user
    
  $scope.$on 'loggedOut', (event) ->
    $scope.loggedIn = false
    
  $scope.logOut = session.logOut
]
