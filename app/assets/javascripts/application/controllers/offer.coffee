angular.module 'PlatMaison'

.controller 'Offer', ['$fileUploader', '$scope', 'api', 'session', 'url', 'util', ($fileUploader, $scope, api, session, url, util) ->
  return if session.notAuthenticated()
  
  $scope.url = url
  
  $scope.data = {}
  $scope.otherData = {}
  $scope.view = 'form'
  
  api.dish_types.index()
  .success (data) ->
    $scope.types = data.dish_types
  
  api.dishes.cuisine_keywords()
  .success (data) ->
    $scope.cuisine_keywords = data.cuisine_keywords
  
  uploader = $scope.uploader = $fileUploader.create
    scope: $scope
    url: ''
    
  util.initForm()
  
  $scope.validateName = ->
    group = util.resetGroup '#name'
    if $scope.data.name?.length > 1
      return util.groupSuccess group
    util.groupError group
    
  $scope.validateDescription = ->
    group = util.resetGroup '#description'
    if $scope.data.description?.length > 1
      return util.groupSuccess group
    util.groupError group
    
  $scope.validateType = ->
    group = util.resetGroup '#type'
    if $scope.otherData.type?
      return util.groupSuccess group
    util.groupError group
    
  $scope.validatePrice = ->
    group = util.resetGroup '#price'
    if $scope.data.price > 0
      return util.groupSuccess group
    util.groupError group
    
  $scope.validatePhoto = ->
    group = util.resetGroup '#photo'
    if uploader.queue.length > 0
      return util.groupSuccess group
    util.groupError group
    
  validateFields = ->
    $scope.validateName() and $scope.validateDescription() and $scope.validateType() and $scope.validatePrice() and $scope.validatePhoto()
    
  $scope.submit = ->
    if validateFields()
      $scope.view = 'creating'
      $scope.data.type = $scope.otherData.type.id
      $scope.data.cuisine_keywords = $('#cuisine_keywords :checked').map ->
        $(this).data 'keywordId'
      .get()
      
      api.users.dishes.create
        dish: $scope.data
      .success (data) ->
        dish = data.dish
        $scope.dish = dish
        item = uploader.queue[uploader.queue.length - 1]
        item.url = api.users.dishes.addPictureUrl dish.id
        uploader.bind 'success', ->
          $scope.$apply ->
            $scope.view = 'succeeded'
        uploader.uploadItem item
      .error (data) ->
        $scope.view = 'form'
        # addMessage

  return
]
