angular.module 'PlatMaison'

.controller 'Checkout', ['$location', '$scope', '$window', 'api', 'cart', 'session', ($location, $scope, $window, api, cart, session) ->
  return if session.notAuthenticated()
  return $location.url '/' if cart.isEmpty()
  
  $scope.cart = cart
  
  $scope.proceed = ->
    api.users.commands.create
      command:
        availability_id: cart.availability.id
        cook_id: cart.cook.id
        dishes: cart.items.map (item) ->
          id: item.dish.id
          amount: item.amount
    .success (data) ->
      api.users.commands.paymentUrl data.command.id
      .success (data) ->
        $window.location.href = data.url
  
  return
]
