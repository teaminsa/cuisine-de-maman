angular.module 'PlatMaison'

.controller 'Dish', ['$location', '$routeParams', '$scope', 'api', 'cart', 'search', 'util', ($location, $routeParams, $scope, api, cart, search, util) ->
  $scope.cart = cart
  $scope.search = search
  
  cart.clear()
  
  mAvailabilityDate = null
  $availabilityDate = $ '#availabilityDate'
  $availabilities = $ '#availabilities'
  
  updateAvailabilityDate = ->
    if mAvailabilityDate
      $availabilityDate.val mAvailabilityDate.format 'L'
      $scope.dayAvailabilities = (availability for availability in $scope.availabilities when mAvailabilityDate.isSame availability.mStart, 'day')
    else
      $availabilityDate.val ''
      $scope.dayAvailabilities = []
    
  $scope.availabilities = []
  
  setAvailability = (availability) ->
    $scope.availability = cart.availability = availability
    util.processAvailability availability
    mAvailabilityDate = $scope.availability.mStart
    updateAvailabilityDate()
    
  resetAvailability = ->
    setAvailability cart.availability
  
  $scope.changeAvailability = ->
    api.commands.reschedule
      availability_id: $scope.availability.id
      cook_id: cart.cook.id
      dishes: cart.items.map (item) ->
        id: item.dish.id
        amount: item.amount
    .success (data) ->
      validate = ->
        setAvailability data.availability
      if cart.different data
        if confirm 'All dishes are not available, do you still want to reschedule?'
          cart.replace data
          validate()
        else
          resetAvailability()
      else
        validate()
    .error (data) ->
      alert data.error
    
  api.availabilities.show $routeParams.availabilityId
  .success (data) ->
    setAvailability data.availability
  
  api.dishes.show $routeParams.dishId
  .success (data) ->
    dish = data.dish
    api.users.show dish.user.id
    .success (data) ->
      $scope.cook = cart.cook = data.user
      
    api.users.dishes.index dish.user.id
    .success (data) ->
      $scope.dishes = data.dishes
      
      api.users.availabilities.index dish.user.id
      .success (data) ->
        $scope.availabilities = data.availabilities
        util.processAvailability availability for availability in data.availabilities
        updateAvailabilityDate()
        
        datepicker = $availabilityDate.datepicker
          format: 'dd/mm/yyyy'
          onRender: (date) ->
            today = moment date
            tomorrow = today.clone()
            tomorrow.add 1, 'd'
            if $scope.availabilities.some((availability) ->
              (availability.mStart.isSame(today, 'day') or availability.mStart.isAfter(today, 'day')) and availability.mStart.isBefore(tomorrow, 'day')
            )
              ''
            else
              'disabled'
        .data 'datepicker'
        $availabilityDate.change ->
          $scope.$apply ->
            mAvailabilityDate = moment $availabilityDate.val(), 'L', moment.lang()
            updateAvailabilityDate()
            datepicker.hide()
            $availabilities.focus()
        
      cart.add dish
      
      $scope.decAmount = (item) ->
        item.amount = Math.max(item.amount - 1, 1)
      
      $scope.incAmount = (item) ->
        ++item.amount
      
      $scope.addToCart = (dish) ->
        cart.add d for d in $scope.dishes when d.id is dish.id
        
      $scope.order = ->
        if cart.availability and not cart.isEmpty()
          $scope.orderError = ''
          $location.url '/checkout'
        else
          $scope.orderError = 'Your command is invalid!'
      
  .error ->
    $scope.error = 'Wrong dish number'
  
  return
]
