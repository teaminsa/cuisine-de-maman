angular.module 'PlatMaison'

# The cart service allows the user command to travel time and space
.service 'cart', ->
  @items = []
  
  @add = (dish) =>
    return if @items.some (item) -> item.dish.id == dish.id 
    @items.push
      dish: dish
      amount: 1
      
  @clear = =>
    @items = []
    @availability = null
    
  @different = (results) =>
    @items.length isnt results.dishes.length or @items.some (item) ->
      results.dishes.some (dish) ->
        item.dish.id is dish.id and item.amount isnt dish.amount
    
  @isEmpty = =>
    @items.length == 0
    
  @replace = (results) =>
    @availability = results.availability
    @items = @items.filter (item) ->
      found = false
      results.dishes.forEach (dish) ->
        if item.dish.id is dish.id
          item.amount = dish.amount
          found = true
      found
    
  @remove = (item) =>
    pos = @items.indexOf item
    if pos isnt -1
      @items.splice pos, 1
  
  @totalPrice = =>
    sum = 0
    @items.forEach (item) ->
      sum += item.dish.price * item.amount
    Math.round(sum * 100) / 100
    
  return
