angular.module 'PlatMaison'

# The search service manages map search's attributes and geolocation
.service 'search', ['$location', '$rootScope', 'url', ($location, $rootScope, url) ->
  nullTime =
    from: 0
    to: 24 * 60
    str: ''
  @times = [nullTime]
  for hours in [11..22]
    for minutes in [0, 30]
      time = hours * 60 + minutes
      @times.push
        time: time
        from: time - 30
        to: time + 30
        str: if minutes then hours + ':' + minutes else hours
  
  locationMargin = 0.01
  
  geocoder = new google.maps.Geocoder()
  
  search = $location.search()
  
  if search.amount?
    amount = parseInt search.amount
  Object.defineProperty this, 'amount',
    configurable: true
    enumerable: true
    get: -> amount or 1
    set: (value) -> amount = value
    
  date = moment search.date, 'L', moment.lang() if search.date
  
  Object.defineProperty this, 'idate',
    configurable: true
    enumerable: true
    get: -> date and date.format 'L'
    set: (value) ->
      date = value and moment value, 'L', moment.lang()
  
  @interval = if search.interval? then parseInt(search.interval)
  
  @location = search.location
  
  @north = parseFloat search.north if search.north?
  @south = parseFloat search.south if search.south?
  @east = parseFloat search.east if search.east?
  @west = parseFloat search.west if search.west?
  
  @time = nullTime
  if search.time?
    m = parseInt search.time
    @times.forEach (time) =>
      if time.time is m
        @time = time
        false
  
  (($, search) ->
    $.fn.autocomplete = ->
      this.each ->
        $this = $ this
        
        autocomplete = new google.maps.places.Autocomplete this, {}
        
        google.maps.event.addListener autocomplete, 'place_changed', ->
          place = autocomplete.getPlace()
        
          if place.geometry.viewport
            search.setBounds place.geometry.viewport
          else
            search.north = place.geometry.location.lat() + locationMargin
            search.south = place.geometry.location.lat() - locationMargin
            
            search.east = place.geometry.location.lng() + locationMargin
            search.west = place.geometry.location.lng() - locationMargin
          
          $this.change()
  ) jQuery, this
    
  @getBounds = =>
    ne = new google.maps.LatLng @north, @east
    sw = new google.maps.LatLng @south, @west
    new google.maps.LatLngBounds sw, ne
    
  @setBounds = (bounds) =>
    ne = bounds.getNorthEast()
    @north = ne.lat()
    @east = ne.lng()
    sw = bounds.getSouthWest()
    @south = sw.lat()
    @west = sw.lng()
    
  @getIntervalStart = =>
    if date and date.isValid()
      date.unix() + @time.from * 60
    
  @getIntervalEnd = =>
    if date and date.isValid()
      date.unix() + @time.to * 60
    
  @geolocalize = =>
    if navigator.geolocation
      navigator.geolocation.getCurrentPosition (position) =>
        lat = position.coords.latitude
        lng = position.coords.longitude
        geocoder.geocode {
          latLng: new google.maps.LatLng(lat, lng)
        }, (results, status) =>
          if status is google.maps.GeocoderStatus.OK
            result = results[2] or results[1] or results[0]
            if result
              $rootScope.$apply =>
                @setBounds result.geometry.bounds
                @location = result.formatted_address
    
  @submit = =>
    return false if not @north? or not @south? or not @east? or not @west?
    $location.url url.search
    $location.search 'north', @north
    $location.search 'south', @south
    $location.search 'east', @east
    $location.search 'west', @west
    $location.search 'location', @location
    $location.search 'date', @idate if date
    $location.search 'time', @time.time if @time.time
    $location.search 'interval', @interval if @interval
    $location.search 'amount', amount if amount
    true
  
  return
]
