angular.module 'PlatMaison'

# The session services handles
# - log in and log out
# - log in with Facebook
# - storage of session data, in order to be able to refresh pages
# - soon: auto reconnection!
.service 'session', ['$http', '$location', '$rootScope', 'api', ($http, $location, $rootScope, api) ->
  backUrl = null
  
  handleRequest = (data) =>
    $http.post api.prefix + 'sessions', data
    .success @handleResponse
    
  @handleResponse = (data) =>
    @data = data
    api.session = data
    sessionStorage.session = JSON.stringify data
    $rootScope.$broadcast 'loggedIn', @data.user
    $location.url backUrl ? '/'
    backUrl = null
    
  @notAuthenticated = =>
    if !@data?
      backUrl = $location.url()
      $location.url '/login'
      return true
    
  @logIn = (email, password, errorCallback) ->
    handleRequest
      email: email
      password: password
    .error errorCallback
    
  loginFacebook = (callback) ->
    FB.login callback,
      scope: 'public_profile'
  
  @logInWithFacebook = (errorCallback) ->
    loginFacebook (response) ->
      handleRequest
        facebook: response.authResponse.accessToken
      .error errorCallback
    
  @logOut = =>
    @data = null
    api.session = null
    delete sessionStorage.session
    $rootScope.$broadcast 'loggedOut'
    $location.url '/'
    
  if sessionStorage.session?
    @data = JSON.parse sessionStorage.session
    $http.get api.prefix + 'sessions/' + @data.user.id + '?token=' + @data.token
    .success =>
      api.session = @data
      $rootScope.$broadcast 'loggedIn', @data.user
    .error =>
      @logOut()
    
  return
]
