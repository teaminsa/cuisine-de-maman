angular.module 'PlatMaison'

# The util service provides random stuff
.service 'util', ->
  
  # Availabilities
  
  @processAvailability = (availability) ->
    availability.mStart = moment availability.start
    availability.mEnd = moment availability.end
  
  # Forms
  
  @initForm = ->
    $('.ng-view input[title]').tooltip
      placement: 'bottom'
      trigger: 'focus'
    $('.ng-view input').first().focus()
  
  @resetGroup = (selector) ->
    group = $(selector).parent()
    group.children('span').remove()
    group.removeClass 'has-error has-success has-warning'
  
  @groupSuccess = (group) ->
    group.append '<span class="glyphicon glyphicon-ok form-control-feedback"></span>'
    group.next('.alert').addClass 'hide'
    group.addClass 'has-success'
    true
  
  @groupWarning = (group) ->
    group.append '<span class="glyphicon glyphicon-warning-sign form-control-feedback"></span>'
    group.next('.alert').removeClass 'hide'
    group.addClass 'has-warning'
    true
  
  @groupError = (group) ->
    group.append '<span class="glyphicon glyphicon-remove form-control-feedback"></span>'
    group.next('.alert').removeClass 'hide'
    group.addClass 'has-error'
    false
  
  return
