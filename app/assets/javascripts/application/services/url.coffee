angular.module 'PlatMaison'

# The url service provides client-side urls
.service 'url', ->
  @dish = (result) =>
    '/dish/' + result.availability.id + '/' + result.dish.id + '/' + @slugize(result.dish.name + ' by ' + result.cook.first_name)
  
  @search = '/search'
  
  @slugize = (str) ->
    str
    .toLowerCase()
    .replace /\s+/g, '-'
    .replace /[^-\w]/g, ''
    
  return
