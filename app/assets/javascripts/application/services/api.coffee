angular.module 'PlatMaison'

# The api service provides a clear gateway to the webservices
# 
# All actions inside the namespace users take an optional last argument to
# specify the user id. When not given, it is set to the current user id.
# api.users.commands.show(42) -> GET /api/users/XXX/commands/42
# api.users.commands.show(42, 1337) -> GET /api/users/1337/commands/42
.service 'api', ['$http', ($http) ->
  @prefix = '/api/'
  
  session = null
  token = ''
  
  Object.defineProperty this, 'session',
    configurable: true
    enumerable: true
    get: -> session
    set: (value) ->
      session = value
      token = if session
        '?token=' + session.token
      else
        ''
  
  root = (segments...) =>
    @prefix + segments.join('/')
    
  # convenience for services which need authorization
  users = (userId, segments...) =>
    root 'users', userId ? session.user.id, segments...
  
  @availabilities =
    show: (availabilityId) ->
      $http.get root('availabilities', availabilityId)
  
  @commands =
    reschedule: (data) ->
      $http.post root('commands', 'reschedule'), data
  
  @dish_types =
    index: ->
      $http.get root('dish_types')
  
  @dishes =
    cuisine_keywords: ->
      $http.get root('dishes', 'cuisine_keywords')
    
    search: (query) ->
      $http.get root('dishes', 'search') + '?' + (key + '=' + value for key, value of query when typeof value isnt 'undefined').join '&'
    
    show: (dishId) ->
      $http.get root('dishes', dishId)
  
  @users =
    availabilities:
      addDish: (availabilityId, dishId, userId) ->
        $http.post users(userId, 'availabilities', availabilityId, 'dishes', dishId),
          token = session.token
      
      create: (data, userId) ->
        data.token = session.token
        $http.post users(userId, 'availabilities'), data
      
      destroy: (availabilityId, userId) ->
        $http.delete users(userId, 'availabilities', availabilityId) + token
      
      index: (userId) ->
        $http.get users(userId, 'availabilities')
      
      removeDish: (availabilityId, dishId, userId) ->
        $http.delete users(userId, 'availabilities', availabilityId, 'dishes', dishId) + token
      
      update: (availabilityId, data, userId) ->
        data.token = session.token
        $http.put users(userId, 'availabilities', availabilityId), data
    
    commands:
      checkPaymentStatus: (commandId, userId) ->
        $http.get users(userId, 'commands', commandId, 'checkPaymentStatus') + token
      
      create: (data, userId) ->
        data.token = session.token
        $http.post users(userId, 'commands'), data
      
      paymentUrl: (commandId, userId) ->
        $http.get users(userId, 'commands', commandId, 'paymentUrl') + token
    
    dishes:
      addPictureUrl: (dishId, userId) ->
        users(userId, 'dishes', dishId, 'add_picture') + token
      
      create: (data, userId) ->
        data.token = session.token
        $http.post users(userId, 'dishes'), data
      
      index: (userId) ->
        $http.get users(userId, 'dishes')
    
    orders:
      accept: (orderId, userId) ->
        $http.put users(userId, 'orders', orderId, 'accept'),
          token: session.token
      
      decline: (orderId, userId) ->
        $http.put users(userId, 'orders', orderId, 'decline'),
          token: session.token
      
      gift: (orderId, userId) ->
        $http.put users(userId, 'orders', orderId, 'gift'),
          token: session.token
      
      index: (userId) ->
        $http.get users(userId, 'orders') + token
    
    create: (data) ->
      $http.post root('users'), data
    
    show: (userId) ->
      $http.get users(userId)
  
  return
]
