json.orders do
	json.array!(@orders) do |order|
	  json.id order.id
	  json.price order.price
	  json.created_at order.created_at
	  json.updated_at order.updated_at
	  json.status order.command_status.status
	  json.dishes do
	    json.array! order.command_dishes do |command_dish|  
	      json.id command_dish.dish.id
	      json.amount command_dish.amount
	      json.name command_dish.dish.name
	    end
	  end	  
	  json.user do
	    json.id order.user.id
	    json.first_name order.user.first_name
	    json.last_name order.user.last_name
	    json.email order.user.email
	    json.telephone order.user.telephone
	  end
	  json.availability do
		json.start order.availability.start
		json.end order.availability.end
	  end
	end
end
