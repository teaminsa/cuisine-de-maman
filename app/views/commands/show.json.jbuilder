json.command do
	json.id @command.id
	json.status @command.command_status.status
	json.delivery_datetime @command.delivery_datetime
	json.price @command.price
	
	json.availability do
		json.start @command.availability.start
		json.end @command.availability.end
	end
	
	json.cook do
		json.id @command.cook.id
		json.first_name @command.cook.first_name
		json.location do
			json.latitude @command.cook.location.latitude
			json.longitude @command.cook.location.longitude
			json.city @command.cook.location.city
			json.address @command.cook.location.address
			json.informations @command.cook.location.informations
		end
	end
	
	json.dishes do
		json.array!(@command.command_dishes) do |command_dish|  
			json.id command_dish.dish.id
			json.amount command_dish.amount
			json.name command_dish.dish.name			
		end
	end
end