json.commands do
	json.array!(@commands) do |command|
	  json.id command.id
	  json.price command.price
	  json.created_at command.created_at
	  json.updated_at command.updated_at
	  json.status command.command_status.status
		json.availability do
			json.start command.availability.start
			json.end command.availability.end
		end
	  json.dishes do
	    json.array!(command.command_dishes) do |command_dish|  
	      json.id command_dish.dish.id
	      json.amount command_dish.amount
	      json.name command_dish.dish.name      
	    end
	  end
	end
end