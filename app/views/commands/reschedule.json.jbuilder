json.availability do
  json.id @availability.id
  json.start @availability.start
  json.end @availability.end
  
  json.dishes do
    json.array!(@availability.dishes) do |dish|
      json.extract! dish, :id, :name
    end
  end
end

json.dishes do
  json.array!(@dishes) do |dish|
    json.extract! dish, :id, :amount
  end
end
