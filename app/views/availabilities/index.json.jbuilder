json.availabilities do
	json.array!(@availabilities) do |availability|
		json.id availability.id
		json.start availability.start
	 	json.end availability.end
	 	
	 	json.dishes do
	   	json.array!(availability.dishes) do |dish|
	 			json.extract! dish, :id, :name
	   	end
		end
	end
end
