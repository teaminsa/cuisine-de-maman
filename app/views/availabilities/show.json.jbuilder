json.availability do
	json.extract! @availability, :id, :start, :end
	json.dishes do
	  json.array!(@availability.dishes) do |dish|
	    json.id dish.id
	    json.name dish.name
	  end
	end
end
