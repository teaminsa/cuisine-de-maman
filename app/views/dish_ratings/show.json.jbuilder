json.dish_rating do

	json.id = @dish_rating.id
	json.value = @dish_rating.rating.value
	json.comments = @dish_rating.rating.comments

end
