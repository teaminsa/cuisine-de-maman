json.array!(@dish_availabilities) do |dish_availability|
  json.extract! dish_availability, :id
  json.url dish_availability_url(dish_availability, format: :json)
end
