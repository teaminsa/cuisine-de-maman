json.array!(@results) do |result|
  availability = result[:availability]
  dish = result[:dish]
  
  json.cook do
    json.id dish.user.id
    json.first_name dish.user.first_name
    
    if dish.user.location
      json.location do
        json.lat dish.user.location.latitude
        json.lng dish.user.location.longitude
      end
    end
  end
  
  json.dish do
    json.id dish.id
    
    json.description dish.description
    json.name dish.name
    json.price dish.price
    json.type dish.dish_type.name
    
    if dish.main_picture
      json.main_picture dish.main_picture.picture.file.url(:thumb)
    end
  end
  
  json.availability do
    json.id availability.id
    json.start availability.start
    json.end availability.end
  end
end
