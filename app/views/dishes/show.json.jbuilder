json.dish do
	json.id @dish.id
	json.name @dish.name
	json.description @dish.description
	json.price @dish.price
	json.created_at @dish.created_at  
	json.cuisine_keywords do
		json.array!(@dish.cuisine_keywords) do |cuisine_keyword|  
			json.keyword cuisine_keyword.keyword
		end
	end
	
	json.type do
	  json.id @dish.dish_type.id
	  json.name @dish.dish_type.name
	end
	
	json.availabilities do
	  json.array!(@dish.availabilities) do |availability|  	
	    json.id availability.id
	    json.start availability.start
	    json.end availability.end
	  end  
	end
	 
	json.user do
	  json.id @dish.user.id
	  json.first_name @dish.user.first_name
	  json.email @dish.user.email
	end
	 
	if @dish.main_picture
	  json.main_picture do
	    json.url @dish.main_picture.picture.file.url
	    json.thumb @dish.main_picture.picture.file.url(:thumb)
	  end
	end
end
