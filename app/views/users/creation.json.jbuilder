json.session do
	json.token @user.session.token 
end

json.user do
	json.extract! @user, :id, :first_name, :email
	json.created_at @user.created_at
	json.updated_at @user.updated_at
	if !@user.main_picture.nil?
		json.main_picture do
			json.id @user.main_picture.id				
		end
	end
end