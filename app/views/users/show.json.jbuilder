json.user do
	json.id @user.id
	json.first_name @user.first_name
	#json.last_name @user.last_name
	json.created_at @user.created_at
	json.updated_at @user.updated_at
	
	json.user_pictures do
		json.array!(@user.user_pictures) do |user_picture|  
			json.id user_picture.id
			json.url user_picture.picture.file.url
		end
	end

	if !@user.main_picture.nil?
		json.main_picture do
			json.id @user.main_picture.id
			json.url @user.main_picture.picture.file.url
		end
	end
end