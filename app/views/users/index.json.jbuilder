json.users do
	json.array!(@users) do |user|
		json.id user.id
		json.email user.email
		json.created_at user.created_at
		json.updated_at user.updated_at		
		if !user.main_picture.nil?
			json.main_picture do
				json.id user.main_picture.id				
			end
		end
	end
end