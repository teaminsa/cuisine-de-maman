json.array!(@command_dishes) do |command_dish|
  json.extract! command_dish, :id
  json.url command_dish_url(command_dish, format: :json)
end
