json.dish_types do
  json.array!(@dish_types) do |dish_type|
    json.extract! dish_type, :id, :name
  end
end
