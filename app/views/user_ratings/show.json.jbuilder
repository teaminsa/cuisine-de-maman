json.user_rating do

	json.id = @user_rating.id
	json.value = @user_rating.rating.value
	json.comments = @user_rating.rating.comments

end
