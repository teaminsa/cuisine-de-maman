json.cuisine_keywords do
	json.array!(@cuisine_keywords) do |cuisine_keyword|
	  json.id cuisine_keyword.id
	  json.keyword cuisine_keyword.keyword
	end
end