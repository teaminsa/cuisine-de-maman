json.token @session.token

json.user do
  json.extract! @session.user, :id, :first_name, :email
  if !@session.user.main_picture.nil?
    json.main_picture do
      json.id @session.user.main_picture.id       
    end
  end
end
