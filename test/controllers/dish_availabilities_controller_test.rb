require 'test_helper'

class DishAvailabilitiesControllerTest < ActionController::TestCase
  setup do
    @dish_availability = dish_availabilities(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:dish_availabilities)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create dish_availability" do
    assert_difference('DishAvailability.count') do
      post :create, dish_availability: {  }
    end

    assert_redirected_to dish_availability_path(assigns(:dish_availability))
  end

  test "should show dish_availability" do
    get :show, id: @dish_availability
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @dish_availability
    assert_response :success
  end

  test "should update dish_availability" do
    patch :update, id: @dish_availability, dish_availability: {  }
    assert_redirected_to dish_availability_path(assigns(:dish_availability))
  end

  test "should destroy dish_availability" do
    assert_difference('DishAvailability.count', -1) do
      delete :destroy, id: @dish_availability
    end

    assert_redirected_to dish_availabilities_path
  end
end
