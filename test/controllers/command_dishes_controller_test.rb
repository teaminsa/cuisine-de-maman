require 'test_helper'

class CommandDishesControllerTest < ActionController::TestCase
  setup do
    @command_dish = command_dishes(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:command_dishes)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create command_dish" do
    assert_difference('CommandDish.count') do
      post :create, command_dish: {  }
    end

    assert_redirected_to command_dish_path(assigns(:command_dish))
  end

  test "should show command_dish" do
    get :show, id: @command_dish
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @command_dish
    assert_response :success
  end

  test "should update command_dish" do
    patch :update, id: @command_dish, command_dish: {  }
    assert_redirected_to command_dish_path(assigns(:command_dish))
  end

  test "should destroy command_dish" do
    assert_difference('CommandDish.count', -1) do
      delete :destroy, id: @command_dish
    end

    assert_redirected_to command_dishes_path
  end
end
